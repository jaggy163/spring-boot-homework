package ru.mail.mantrov.dao;

import org.springframework.data.repository.CrudRepository;
import ru.mail.mantrov.domain.Player;

import java.util.List;

public interface FootballPlayerDao extends CrudRepository<Player, Long> {
    List<Player> findByClubCityName(String city);
}
