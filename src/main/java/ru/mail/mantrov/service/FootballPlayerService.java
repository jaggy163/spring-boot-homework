package ru.mail.mantrov.service;

import ru.mail.mantrov.domain.Player;

import java.util.List;

public interface FootballPlayerService extends Service<Player> {
    List<Player> findAllPlayersFromCity(String cityName);
}
