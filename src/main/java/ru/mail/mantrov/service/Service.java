package ru.mail.mantrov.service;

import java.util.List;

public interface Service<O> {

    List<O> findAll();
    void create(O o);
    O findBy(long id);
    void update(O o);
    void delete(long id);
}
