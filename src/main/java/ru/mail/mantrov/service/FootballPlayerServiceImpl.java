package ru.mail.mantrov.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.mail.mantrov.dao.FootballPlayerDao;
import ru.mail.mantrov.domain.Player;
import ru.mail.mantrov.errors.NotFoundException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@org.springframework.stereotype.Service
public class FootballPlayerServiceImpl implements FootballPlayerService {

    @Autowired
    private FootballPlayerDao dao;

    @Override
    public void update(Player player) {
        Player playerToUpdate = findBy(player.getId());
        if (player.getFirstName() != null) {
            playerToUpdate.setFirstName(player.getFirstName());
        }
        if (player.getSecondName() != null) {
            playerToUpdate.setSecondName(player.getSecondName());
        }
        if (player.getPlayerInfo() != null) {
            playerToUpdate.setPlayerInfo(player.getPlayerInfo());
        }
        if (player.getClub() != null) {
            playerToUpdate.setClub(player.getClub());
        }

        dao.deleteById(player.getId());
        dao.save(playerToUpdate);
    }

    @Override
    public List<Player> findAll() {
        return StreamSupport.stream(dao.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Player findBy(long id) {
        Player player;
        try {
            player = dao.findById(id).get();
        } catch (NoSuchElementException e) {
            throw new NotFoundException(id);
        }
        return player;
    }

    @Override
    public void create(Player player) {
        dao.save(player);
    }

    @Override
    public void delete(long id) {
        dao.deleteById(id);
    }

    @Override
    public List<Player> findAllPlayersFromCity(String cityName) {
        return dao.findByClubCityName(cityName);
    }
}
