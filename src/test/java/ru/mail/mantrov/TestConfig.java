package ru.mail.mantrov;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import ru.mail.mantrov.domain.Player;

import java.time.LocalDate;

@TestConfiguration
public class TestConfig {

    @Bean
    public Player playerBuilder() {
        return new TestPlayerBuilder()
                .setCityName("Samara")
                .setClubTitle("Krylya Sovetov")
                .setCompetitionName("RFPL")
                .setPlayersFirstName("Alexander")
                .setPlayersSecondName("Sobolev")
                .setBirthDate(LocalDate.of(1997, 3, 7))
                .setNumber(7)
                .build();
    }
}
