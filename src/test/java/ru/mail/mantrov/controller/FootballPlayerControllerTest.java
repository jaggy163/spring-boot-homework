package ru.mail.mantrov.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.mail.mantrov.TestConfig;
import ru.mail.mantrov.domain.Player;
import ru.mail.mantrov.service.FootballPlayerService;

import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.doNothing;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@Import(TestConfig.class)
@WebMvcTest(FootballPlayerController.class)
public class FootballPlayerControllerTest {
    private static final int ID = 0;
    private static final String CITY = "Samara";

    @Autowired
    private Player player;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mock;

    @MockBean
    private FootballPlayerService service;


    @Test
    public void findAllPlayers() throws Exception {
        List<Player> players = Collections.singletonList(player);
        given(service.findAll()).willReturn(players);
        mock.perform(get("/players/list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(players)));
        verify(service).findAll();
    }

    @Test
    public void findBy() throws Exception {
        given(service.findBy(ID)).willReturn(player);

        mock.perform(get("/players/id={id}", ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(player)));

        verify(service).findBy(ID);
    }

    @Test
    public void findAllPlayersFromCity() throws Exception {
        List<Player> players = Collections.singletonList(player);
        given(service.findAllPlayersFromCity(CITY)).willReturn(players);
        mock.perform(get("/players/city={city}", CITY))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(players)));
        verify(service).findAllPlayersFromCity(CITY);
    }

    @Test
    public void createPlayer() throws Exception {
        doNothing().when(service).create(player);
        mock.perform(post("/players")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(player)))
                .andDo(print())
                .andExpect(status().isCreated());
        verify(service).create(player);
    }

    @Test
    public void deletePlayer() throws Exception {
        doNothing().when(service).delete(ID);
        mock.perform(delete("/players/id={id}", ID))
                .andDo(print())
                .andExpect(status().isNoContent());
        verify(service).delete(ID);
    }

    @Test
    public void updatePlayer() throws Exception {
        doNothing().when(service).update(player);
        mock.perform(put("/players")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(player)))
                .andDo(print())
                .andExpect(status().isOk());
        verify(service).update(player);
    }
}