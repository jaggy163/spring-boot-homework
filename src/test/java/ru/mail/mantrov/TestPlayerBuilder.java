package ru.mail.mantrov;

import ru.mail.mantrov.domain.*;

import java.time.LocalDate;
import java.util.Collections;

public class TestPlayerBuilder {
    private String cityName;
    private String clubTitle;
    private String competitionName;
    private String playersFirstName;
    private String playersSecondName;
    private LocalDate birthDate;
    private int number;

    public Player build() {
        City city = new City();
        city.setName(cityName);

        Club club = new Club();
        club.setCity(city);
        club.setTitle(clubTitle);

        Competition competition = new Competition();
        competition.setCompetitionName(competitionName);
        competition.setClubs(Collections.singleton(club));

        club.setCompetitions(Collections.singleton(competition));

        Player player = new Player();
        player.setFirstName(playersFirstName);
        player.setSecondName(playersSecondName);

        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.setBirthDate(birthDate);
        playerInfo.setNumber(number);
        playerInfo.setPlayer(player);

        player.setPlayerInfo(playerInfo);

        return player;
    }

    public TestPlayerBuilder setCityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public TestPlayerBuilder setClubTitle(String clubTitle) {
        this.clubTitle = clubTitle;
        return this;
    }

    public TestPlayerBuilder setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
        return this;
    }

    public TestPlayerBuilder setPlayersFirstName(String playersFirstName) {
        this.playersFirstName = playersFirstName;
        return this;
    }

    public TestPlayerBuilder setPlayersSecondName(String playersSecondName) {
        this.playersSecondName = playersSecondName;
        return this;
    }

    public TestPlayerBuilder setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TestPlayerBuilder setNumber(int number) {
        this.number = number;
        return this;
    }
}
